#ifndef REGISTERS_H_
#define REGISTERS_H_

#include <stdint.h>

typedef enum{
	PORT_GPIO0 = 0,
	PORT_GPIO1 = 1
}PORT_GPIO_t;

typedef enum{
	INPUT = 0,
	OUTPUT = 1
}DIR_t;

enum{
	GPIO0_0 = 0,
	GPIO0_1,
	GPIO0_2,
	GPIO0_3,
	GPIO0_4,
	GPIO0_5,
	GPIO0_6,
	GPIO0_7,
	GPIO0_8,
	GPIO0_9,
	GPIO0_10,
	GPIO0_11,
	GPIO0_12,
	GPIO0_13,
	GPIO0_14,
	GPIO0_15,
	GPIO0_16,
	GPIO0_17,
	GPIO0_18,
	GPIO0_19,
	GPIO0_20,
	GPIO0_21,
	GPIO0_22,
	GPIO0_23,
	GPIO0_24,
	GPIO0_25,
	GPIO0_26,
	GPIO0_27,
	GPIO0_28,
	GPIO0_29,
	GPIO0_30,
	GPIO0_31
};
enum{
	GPIO1_0 = 0,
	GPIO1_1,
	GPIO1_2,
	GPIO1_3,
	GPIO1_4,
	GPIO1_5,
	GPIO1_6,
	GPIO1_7,
	GPIO1_8,
	GPIO1_9,
	GPIO1_10,
	GPIO1_11,
	GPIO1_12,
	GPIO1_13,
	GPIO1_14,
	GPIO1_15,
	GPIO1_16,
	GPIO1_17,
	GPIO1_18,
	GPIO1_19,
	GPIO1_20,
	GPIO1_21
};

typedef struct{
	unsigned int : 1;
	unsigned int : 1;
	unsigned int : 1;
	unsigned int : 1;
	unsigned int : 1;
	unsigned int I2C0 : 1;
	unsigned int GPIO0 : 1;
	unsigned int : 1;
	unsigned int SCT : 1;
	unsigned int WKT : 1;
	unsigned int MRT : 1;
	unsigned int SPI0 : 1;
	unsigned int SPI1 : 1;
	unsigned int CRC : 1;
	unsigned int UART0 : 1;
	unsigned int UART1 : 1;
	unsigned int UART2 : 1;
	unsigned int WWDT : 1;
	unsigned int IOCON : 1;
	unsigned int ACMP : 1;
	unsigned int GPIO1 : 1;
	unsigned int I2C1 : 1;
	unsigned int I2C2 : 1;
	unsigned int I2C3 : 1;
	unsigned int ADC : 1;
	unsigned int CTIMER0 : 1;
	unsigned int MTB : 1;
	unsigned int DAC0 : 1;
	unsigned int GPIO_INT : 1;
	unsigned int DMA : 1;
	unsigned int UART3 : 1;
	unsigned int UART4 : 1;
}SYSCON_SYSAHBCLKCTRL0_t;



#define SYSCON_BASE_ADDR (0x40048000)
#define GPIO_BASE_ADDR (0xA0000000)

#define SYSCON_SYSAHBCLKCTRL0_ADDR ( (volatile SYSCON_SYSAHBCLKCTRL0_t *)(SYSCON_BASE_ADDR + 0x80))

#define GPIO_DIR0_ADDR (*(volatile unsigned int *)(GPIO_BASE_ADDR + 0x2000))
#define GPIO_CLR0_ADDR (*(volatile unsigned int *)(GPIO_BASE_ADDR + 0x2280))
#define GPIO_SET0_ADDR (*(volatile unsigned int *)(GPIO_BASE_ADDR + 0x2200))

#define GPIO_DIR1_ADDR (*(volatile unsigned int *)(GPIO_BASE_ADDR + 0x2004))
#define GPIO_CLR1_ADDR (*(volatile unsigned int *)(GPIO_BASE_ADDR + 0x2284))
#define GPIO_SET1_ADDR (*(volatile unsigned int *)(GPIO_BASE_ADDR + 0x2204))

void SetPin(PORT_GPIO_t PORT, unsigned int PIN);
void ClearPin(PORT_GPIO_t PORT, unsigned int PIN);
void SetDir(PORT_GPIO_t PORT, unsigned int PIN, DIR_t DIR);

void SetNibble(PORT_GPIO_t PORT, unsigned int PIN, unsigned char value);

void Initialize(void);



#endif /* REGISTERS_H_ */

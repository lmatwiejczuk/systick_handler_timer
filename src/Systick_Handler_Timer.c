#include <cr_section_macros.h>
#include "Timer.h"
#include "Registers.h"

volatile static uint32_t msTicks=0;

void SysTick_Handler(void){
	msTicks++;
}

static void __Delay_ms(uint32_t DELAY_TIME)
{
uint32_t CURRENT_TICKS;
CURRENT_TICKS = msTicks;
while ((msTicks - CURRENT_TICKS) < DELAY_TIME);
}

int main(void) {
	Initialize();
	Config_Timer();
	SetDir(PORT_GPIO1, GPIO1_1, OUTPUT);
    while(1) {
    	ClearPin(PORT_GPIO1, GPIO1_1);
    	__Delay_ms(1000);
    	SetPin(PORT_GPIO1, GPIO1_1);
    	__Delay_ms(1000);
    }
    return 0 ;
}



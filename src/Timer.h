#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>

typedef struct{
	unsigned int ENABLE : 1;
	unsigned int TICKINT : 1;
	unsigned int CLKSOURCE : 1;
	unsigned int : 13;
	unsigned int COUNTFLAG : 1;
	unsigned int : 15;
}SYSTICK_CSR_t;

#define SYSTICK_BASE_ADDR (0xE000E000)

#define SYSTICK_CSR ((volatile SYSTICK_CSR_t *)(SYSTICK_BASE_ADDR + 0x010))
#define SYSTICK_RVR (*(volatile unsigned int *)(SYSTICK_BASE_ADDR + 0x014))
#define SYSTICK_CVR (*(volatile unsigned int *)(SYSTICK_BASE_ADDR + 0x018))

void Config_Timer(void);


#endif /* TIMER_H_ */
